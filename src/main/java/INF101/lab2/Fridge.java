package INF101.lab2;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    ArrayList <FridgeItem> items= new ArrayList<>();

    public Fridge(){
        items = new ArrayList<>();
    }
    
    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()){
            items.add(item);
            return true;
        }
        else{
        // TODO Auto-generated method stub
            return false;}
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge()>0){
            if (items.contains(item)){
                items.remove(item);
            }
            else {
                throw new NoSuchElementException();
            }
        } else{
            throw new NoSuchElementException();
        }
        // TODO Auto-generated method stub
        
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList <FridgeItem> goodFood = new ArrayList <>();
        ArrayList <FridgeItem> expiredFood = new ArrayList <>();
        for (FridgeItem food:items){
            if (food.hasExpired()){
                expiredFood.add(food);
            }
            else {goodFood.add(food);}

        }
        items.clear();
        items.addAll(goodFood);
        
        // TODO Auto-generated method stub
        return expiredFood;
    }
    
}
